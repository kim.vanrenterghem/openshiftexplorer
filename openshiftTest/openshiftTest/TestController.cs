﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace openshiftTest
{
    [Route("api/test")]
    public class TestController
    {
        [HttpGet("{id}")]
        public string get(string id)
            => "halloWorld";
        
        [HttpGet]
        public IEnumerable<string> get()
            => new[]{"hallo","World"};
    }
}